const _overlay = document.createElement('div');
_overlay.classList.add('no-interruptions-extension-overlay');

setTimeout(() => {
  const VIDEO = document.querySelector('video');
  toggleOverlay();
  VIDEO.addEventListener('playing', () => toggleOverlay('play'));
  VIDEO.addEventListener('pause', () => toggleOverlay('pause'));
}, 5000);

function toggleOverlay(action = '') {
  const CONTAINER = document.querySelector('#secondary');
  const SUGGESTED = document.querySelector('#secondary-inner');
  const OVERLAY = document.querySelector('.no-interruptions-extension-overlay');
  const args = [CONTAINER, SUGGESTED, OVERLAY];
  switch (action) {
    case 'play':
      createOverlay(...args);

      break;
    case 'pause':
      removeOverlay(...args);
      break;
    default:
      if (OVERLAY) {
        removeOverlay(...args);
      } else {
        createOverlay(...args);
      }
      break;
  }
}

function createOverlay(...elements) {
  const [container, suggested, overlay] = elements;
  if (overlay) return;
  container.appendChild(_overlay);
  suggested.style.opacity = 0.25;
  suggested.style.filter = 'blur(5px)';
  container.style.position = 'relative';
}

function removeOverlay(...elements) {
  const [container, suggested, overlay] = elements;
  if (!overlay) return;
  container.removeChild(_overlay);
  container.removeAttribute('style');
  suggested.removeAttribute('style');
}

chrome.runtime.onMessage.addListener(gotMessage);
function gotMessage(message, sender, sendResponse) {
  toggleOverlay();
}
