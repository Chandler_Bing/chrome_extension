const BUTTON = document.querySelector('.btn');

function handleClick() {
  const params = {
    active: true,
    currentWindow: true,
  };
  chrome.tabs.query(params, tabs => {
    const message = {
      on: true,
    };
    chrome.tabs.sendMessage(tabs[0].id, message);
  });
}

BUTTON.addEventListener('click', handleClick);

// const INPUTS = document.querySelectorAll('input');
// window.onload = () => {
//   console.log('onload' + Date());
// };
// function changed() {
//   const params = {
//     active: true,
//     currentWindow: true,
//   };
//   let count =
//     chrome.storage.sync.get(['count'], function(result) {
//       console.log('Value currently is ', result);
//       return result.count;
//     }) || 0;

//   chrome.storage.sync.set({ count: count + 1 }, function(res) {
//     // console.log('Value is set to ' + count);
//     console.log({ res });
//     return count + 1;
//   });
//   console.log('third', { count });
//   chrome.tabs.query(params, tabs => {
//     const val = [...INPUTS].reduce((acc, input) => {
//       acc[input.name] = input.checked;
//       return acc;
//     }, {});
//     const message = {
//       txt: "how you doin'?",
//       val,
//     };
//     chrome.tabs.sendMessage(tabs[0].id, message);
//     chrome.extension.getBackgroundPage();
//   });
// }
// INPUTS.forEach(input => {
//   input.addEventListener('change', changed);
// });
